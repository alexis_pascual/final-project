import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule, Routes } from '@angular/router';

import { AngularFireModule } from 'angularfire2';

import { AppComponent } from './app.component';
import { LoginPageComponent } from './login-page/login-page.component';
import { HomePageComponent } from './home-page/home-page.component';

import { AuthService } from './providers/auth.service';
import { CreateProfileComponent } from './create-profile/create-profile.component';

export const firebaseConfig = {
    apiKey: "AIzaSyAztAkCLt4MCUJn290aNKSOtmf8m-ILPfM",
    authDomain: "finalproject-70aa1.firebaseapp.com",
    databaseURL: "https://finalproject-70aa1.firebaseio.com",
    projectId: "finalproject-70aa1",
    storageBucket: "finalproject-70aa1.appspot.com",
    messagingSenderId: "919953343752"
}

const routes: Routes = [
    {path: '', component: HomePageComponent},
    {path: 'login', component: LoginPageComponent},
    {path: 'create', component: CreateProfileComponent}
]

@NgModule({
  declarations: [
    AppComponent,
    LoginPageComponent,
    HomePageComponent,
    CreateProfileComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    AngularFireModule.initializeApp(firebaseConfig),
    RouterModule.forRoot(routes)
  ],
  providers: [AuthService],
  bootstrap: [AppComponent]
})
export class AppModule { }
