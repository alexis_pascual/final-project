import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../providers/auth.service';
import { AppComponent } from '../app.component';
import { AngularFire, AuthProviders, AuthMethods } from 'angularfire2';

declare var firebase: any;

@Component({
  selector: 'app-create-profile',
  templateUrl: './create-profile.component.html',
  styleUrls: ['./create-profile.component.css']
})
export class CreateProfileComponent implements OnInit {

    email: any;
    userid: string;

    constructor(public af: AngularFire) {
        this.af.auth.subscribe(auth => {
            console.log(auth.uid);
            this.userid = auth.uid;
            this.email = af.auth.getAuth().auth.email;
            });

        const profile$ = af.database.list('profile');

        profile$.subscribe(
            val => console.log(val)
        );
    }

    ngOnInit() {

    }

    dota2 = null;
    csgo = null;
    destiny = null;
    battlefield = null;
    desktop = null;
    playstation = null;
    Xbox = null;

    fbPostData(userid, email, name, age, microphones, dota2, csgo, destiny, battlefield, desktop, playstation, Xbox, playstyle) {
        const newId = firebase.database().ref('/profile').push({userid: userid, email: email, name: name, age: age, microphones: microphones, games: { dota2, csgo, destiny, battlefield }, platforms: { desktop, playstation, Xbox }, playstyle: playstyle}).key;
        console.log(newId);
    }



}
