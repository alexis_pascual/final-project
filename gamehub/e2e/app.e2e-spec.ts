import { GamehubPage } from './app.po';

describe('gamehub App', () => {
  let page: GamehubPage;

  beforeEach(() => {
    page = new GamehubPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
